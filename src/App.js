import React from 'react';
import { Observable, Subject, pipe } from 'rxjs';
import { debounceTime } from 'rxjs/operators'

import './App.css';

class App extends React.Component {
    subscription;

    constructor() {
        super();
        this.monitor$ = new Subject();
        this.monitor = this.monitor$.asObservable().pipe(
            debounceTime(500),
        );

        this.state = {
            temperature: NaN,
            airPressure: NaN,
            humidity: NaN,
        };
    }

    componentDidMount() {
        // this.subscription = this.monitor.subscribe(() => {
            this.eventMonitor();
        // });
    }

    componentWillUnmount() {
        this.subscription.unsubscribe();
    }

    eventMonitor() {
        const temperature = Observable.create(function(observer) {
            let value = (Math.random() * (100)).toFixed(2);
            let rnd = Math.random() * (2000 - 100) + 100;

            const interval = setInterval(() => {
                rnd = Math.random() * (2000 - 100) + 100;
                observer.next(value);
                value = rnd > 1000 ? NaN : (Math.random() * (100)).toFixed(2);

                }, rnd);

            return () => clearInterval(interval);
        });

        const airPressure = Observable.create(function(observer) {
            let value = (Math.random() * (100)).toFixed(2);
            let rnd = Math.random() * (2000 - 100) + 100;

            const interval = setInterval(() => {
                rnd = Math.random() * (2000 - 100) + 100;
                observer.next(value);
                value = rnd > 1000 ? NaN : (Math.random() * (100)).toFixed(2);

            }, rnd);

            return () => clearInterval(interval);
        });

        const humidity = Observable.create(function(observer) {
            let value = (Math.random() * (100)).toFixed(2);
            let rnd = Math.random() * (2000 - 100) + 100;

            const interval = setInterval(() => {
                rnd = Math.random() * (2000 - 100) + 100;
                observer.next(value);
                value = rnd > 1000 ? NaN : (Math.random() * (100)).toFixed(2);

            }, rnd);

            return () => clearInterval(interval);
        });

        temperature.subscribe(val => {this.setState( { temperature: val })});
        airPressure.subscribe(val => {this.setState( { airPressure: val })});
        humidity.subscribe(val => {this.setState( { humidity: val })});
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <p>
                        Temperature: {this.state.temperature}
                    </p>
                    <p>
                        Air Pressure: {this.state.airPressure}
                    </p>
                    <p>
                        Humidity: {this.state.humidity}
                    </p>
                </header>
            </div>
        );
    }
}

export default App;


// export for unit test

// export const simpleMapTest = (observable$) => {
//   return observable$.pipe(
//     map(data => data + ' search'),
//   )
// };


//unit test

//it('should change value of the observable', (done) => {
//   const observable$ = of('text');
//   simpleMapTest(observable$).subscribe((value) => {
//     expect(value).toBe('value ');
//     done();
//   });
// });
